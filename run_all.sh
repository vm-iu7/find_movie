cd fm_frontend
screen -S front -d -m sbt 'run 9000'

cd ../fm_movies
screen -S movies -d -m sbt 'run 9001'

cd ../fm_users
screen -S users -d -m sbt 'run 9002'

cd ../fm_session
screen -S session -d -m sbt 'run 9003'

cd ../fm_recommendations
screen -S rec -d -m sbt 'run 9004'

cd ../fm_frontend
screen -S gulp -d -m gulp

cd ..
screen -S redis -d -m redis-server
