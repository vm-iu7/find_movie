import re

p = re.compile(ur'(?P<id>\d+),\s*"?(?P<name>[^\(]+\((?P<year>\d+)\))"?,.+')

input_file = open('./ml-latest-small/movies.csv', 'r')

with open('movies_years.csv', 'w') as out_file:
    for line in input_file:
        m = re.match(p, line)
        if (m):
            m_id = m.group('id')
            name = m.group('name')
            year = m.group('year')
            out_file.write('{},"{}",{}\n'.format(m_id, name, year))
