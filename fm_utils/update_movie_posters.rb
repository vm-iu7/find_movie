require 'rubygems'
require 'sequel'
require 'csv'

DB = Sequel.connect('mysql://play2:123@localhost:3306/fm_movies')

CSV.foreach('posters.csv') do |row|

  movie_id = row[0].to_i
  tmdb_poster_path = row[1]

  print("#{[movie_id, tmdb_poster_path]}\n")

  query = "
    UPDATE `MOVIES`
    SET `tmdb_poster_path` = '#{tmdb_poster_path}'
    WHERE `id` = #{movie_id}
  "

  DB.run(query) unless (movie_id == 0) || (!tmdb_poster_path)

end

movies = DB['SELECT `id`, `tmdb_poster_path` FROM MOVIES LIMIT 15'].all

puts movies

DB.close()
