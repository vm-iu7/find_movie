require 'rubygems'
require 'sequel'
require 'csv'

DB = Sequel.connect('mysql://play2:123@localhost:3306/fm_movies')

CSV.foreach('ml-latest-small/links.csv') do |row|

  movie_id = row[0].to_i
  imdb_id = row[1].to_i
  tmdb_id = row[2].to_i

  query = "
    UPDATE `MOVIES`
    SET `imdb_id` = #{imdb_id}, `tmdb_id` = #{tmdb_id}
    WHERE `id` = #{movie_id}
  "

  DB.run(query) unless (movie_id == 0) || (imdb_id == 0) || (tmdb_id == 0)

end

movies = DB['SELECT `id`, `imdb_id` FROM MOVIES LIMIT 15'].all

puts movies

DB.close()
