USE fm_movies;

drop table MOVIES;

USE fm_movies;

CREATE TABLE MOVIES (
  id INT AUTO_INCREMENT NOT NULL,
  name VARCHAR(255) NOT NULL,
  description TEXT NULL,
  PRIMARY KEY(id)
);

CREATE INDEX idx_movies_name ON MOVIES (name);