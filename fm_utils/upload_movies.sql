USE fm_movies;

load data local infile '/Users/vardan/projects/univer/dist-sys/find_movie/fm_utils/ml-latest-small/movies_years.csv'
into table MOVIES
fields
	terminated by ','
    enclosed by '"'
lines
	terminated by '\n'
(`id`, `name`, `year`);

select * from MOVIES;
