require 'rubygems'
require 'sequel'
require 'themoviedb'

DB = Sequel.connect('mysql://play2:123@localhost:3306/fm_movies')

api_key = ENV['TMDB_TOKEN']
Tmdb::Api.key(api_key)

puts api_key

movies = DB[:movies]

movies.each do |movie|
  if (movie[:tmdb_id])
    tmdb_movie = Tmdb::Movie.detail(movie[:tmdb_id])
    poster_path = tmdb_movie['poster_path']
    print("#{movie[:id]}, #{poster_path}\n")
  end
end
