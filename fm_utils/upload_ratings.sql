use mr_recommendations;

load data local infile '/Users/vardan/Downloads/ml-latest-small/ratings.csv' 
into table RATINGS
fields 
	terminated by ',' 
    enclosed by '"'
lines 
	terminated by '\n'
ignore 1 lines
(`user_id`, `movie_id`, `rating`, @dummy);

select * from RATINGS;
